FROM alpine

# Установка SSH сервера
RUN apk add openssh
RUN ssh-keygen -A

# Установка пароля root
RUN echo "root:PUK" | chpasswd

# Создание sudoers пользователя tester с паролем puk
RUN apk add sudo
RUN adduser -D tester
RUN echo "tester ALL=(ALL) ALL" > /etc/sudoers.d/tester && chmod 0440 /etc/sudoers.d/tester
RUN echo "tester:puk" | chpasswd

EXPOSE 22
# Копирование файла test.sh в образ
COPY test.sh /tell_me_mem.sh

CMD ["/usr/sbin/sshd", "-D"]