#!/bin/sh

# Путь к файлу
filename="kek.log"

# Основной цикл
while true; do
    # Получаем текущее время
    current_time=$(date +"[%Y-%m-%d %H:%M:%S]")

    # Дописываем строку в файл
    echo "$current_time i'm working" >> $filename
    echo "$current_time funny meme: kek" >> $filename
    echo "it was funny"

    # Ждем 1 секунду
    sleep 1
done